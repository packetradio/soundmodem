# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the soundmodem package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: soundmodem 0.20-rsa5\n"
"Report-Msgid-Bugs-To: roland.schwarz@blackspace.at\n"
"POT-Creation-Date: 2022-01-05 16:59+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: configapp/src/app_menu.ui:7
msgid "_About"
msgstr ""

#: configapp/src/app_menu.ui:11
msgid "_Preferences"
msgstr ""

#: configapp/src/app_menu.ui:17
msgid "_Quit"
msgstr ""

#: configapp/src/configappwin.cc:656
msgid "Could not open config file"
msgstr ""

#: configapp/src/gears_menu.ui:7
msgid "New _Configuration"
msgstr ""

#: configapp/src/gears_menu.ui:11
msgid "New Cha_nnel"
msgstr ""

#: configapp/src/gears_menu.ui:15 configapp/src/window.ui:216
msgid "Delete Item"
msgstr ""

#: configapp/src/gears_menu.ui:21
msgid "_Save"
msgstr ""

#: configapp/src/gears_menu.ui:25
msgid "Save as ..."
msgstr ""

#: configapp/src/window.ui:13
msgid "Copyright © 2022 Roland Schwarz OE1RSA"
msgstr ""

#: configapp/src/window.ui:198
msgid "New Configuration"
msgstr ""

#: configapp/src/window.ui:207
msgid "New Channel"
msgstr ""

#: configapp/src/window.ui:321 configapp/src/window.ui:981
#: configapp/src/window.ui:1429 configapp/src/window.ui:1975
msgid "Mode"
msgstr ""

#: configapp/src/window.ui:335 configapp/src/window.ui:453
msgid "alsa"
msgstr ""

#: configapp/src/window.ui:336 configapp/src/window.ui:628
msgid "file"
msgstr ""

#: configapp/src/window.ui:337 configapp/src/window.ui:687
msgid "soundcard"
msgstr ""

#: configapp/src/window.ui:338 configapp/src/window.ui:569
msgid "simulation"
msgstr ""

#: configapp/src/window.ui:369
msgid "ALSA Audio Driver"
msgstr ""

#: configapp/src/window.ui:381 configapp/src/window.ui:654
msgid "Half Duplex"
msgstr ""

#: configapp/src/window.ui:393
msgid "Capture Channel"
msgstr ""

#: configapp/src/window.ui:417
msgid "Mono"
msgstr ""

#: configapp/src/window.ui:418
msgid "Left"
msgstr ""

#: configapp/src/window.ui:419
msgid "Right"
msgstr ""

#: configapp/src/window.ui:434
msgid "default"
msgstr ""

#: configapp/src/window.ui:435
msgid "plughw:0,0"
msgstr ""

#: configapp/src/window.ui:441
msgid "plughw:0,1"
msgstr ""

#: configapp/src/window.ui:468
msgid "Channel Type"
msgstr ""

#: configapp/src/window.ui:480
msgid "Noise Attenuation"
msgstr ""

#: configapp/src/window.ui:492
msgid "Noise Mode"
msgstr ""

#: configapp/src/window.ui:504
msgid "Sampling Rate"
msgstr ""

#: configapp/src/window.ui:517
msgid "0 - Ideal channel"
msgstr ""

#: configapp/src/window.ui:518
msgid "1 - Measured channel (@11025SPS)"
msgstr ""

#: configapp/src/window.ui:519
msgid "2 - Measured channel (@11025SPS)"
msgstr ""

#: configapp/src/window.ui:557
msgid "0 - White Gaussian"
msgstr ""

#: configapp/src/window.ui:558
msgid "1 - f^2 (Limiter Discriminator)"
msgstr ""

#: configapp/src/window.ui:584
msgid "Input File"
msgstr ""

#: configapp/src/window.ui:595
msgid "Repeat"
msgstr ""

#: configapp/src/window.ui:643
msgid "Audio Driver"
msgstr ""

#: configapp/src/window.ui:710
msgid "PTT Driver"
msgstr ""

#: configapp/src/window.ui:722
msgid "Hamlib model"
msgstr ""

#: configapp/src/window.ui:734
msgid "Rig Configuration Parameters"
msgstr ""

#: configapp/src/window.ui:760
msgid "Select Serial Port"
msgstr ""

#: configapp/src/window.ui:811
msgid "TxDelay"
msgstr ""

#: configapp/src/window.ui:823
msgid "Slot Time"
msgstr ""

#: configapp/src/window.ui:835
msgid "P-Persistence"
msgstr ""

#: configapp/src/window.ui:847
msgid "Full Duplex"
msgstr ""

#: configapp/src/window.ui:859
msgid "Tx Tail"
msgstr ""

#: configapp/src/window.ui:995 configapp/src/window.ui:1032
#: configapp/src/window.ui:1443 configapp/src/window.ui:1482
msgid "Off"
msgstr ""

#: configapp/src/window.ui:996 configapp/src/window.ui:1142
#: configapp/src/window.ui:1444 configapp/src/window.ui:1596
msgid "afsk"
msgstr ""

#: configapp/src/window.ui:997 configapp/src/window.ui:1209
#: configapp/src/window.ui:1445 configapp/src/window.ui:1666
msgid "fsk"
msgstr ""

#: configapp/src/window.ui:998 configapp/src/window.ui:1226
#: configapp/src/window.ui:1447 configapp/src/window.ui:1752
msgid "pam"
msgstr ""

#: configapp/src/window.ui:999 configapp/src/window.ui:1243
#: configapp/src/window.ui:1448 configapp/src/window.ui:1769
msgid "psk"
msgstr ""

#: configapp/src/window.ui:1000 configapp/src/window.ui:1387
#: configapp/src/window.ui:1449 configapp/src/window.ui:1913
msgid "newqpsk"
msgstr ""

#: configapp/src/window.ui:1047 configapp/src/window.ui:1158
#: configapp/src/window.ui:1259 configapp/src/window.ui:1497
#: configapp/src/window.ui:1612 configapp/src/window.ui:1682
#: configapp/src/window.ui:1785
msgid "Bit per Second"
msgstr ""

#: configapp/src/window.ui:1059 configapp/src/window.ui:1509
msgid "Frequency 0"
msgstr ""

#: configapp/src/window.ui:1071 configapp/src/window.ui:1521
msgid "Frequency 1"
msgstr ""

#: configapp/src/window.ui:1083
msgid "Differential Encoding"
msgstr ""

#: configapp/src/window.ui:1170 configapp/src/window.ui:1624
#: configapp/src/window.ui:1694
msgid "Filter Curve"
msgstr ""

#: configapp/src/window.ui:1195 configapp/src/window.ui:1652
#: configapp/src/window.ui:1721
msgid "df9ic/g3ruh"
msgstr ""

#: configapp/src/window.ui:1196 configapp/src/window.ui:1653
#: configapp/src/window.ui:1722
msgid "rootraisedcosine"
msgstr ""

#: configapp/src/window.ui:1197 configapp/src/window.ui:1654
#: configapp/src/window.ui:1723
msgid "raisedcosine"
msgstr ""

#: configapp/src/window.ui:1198 configapp/src/window.ui:1655
#: configapp/src/window.ui:1724
msgid "hamming"
msgstr ""

#: configapp/src/window.ui:1271 configapp/src/window.ui:1797
msgid "Interleave"
msgstr ""

#: configapp/src/window.ui:1283 configapp/src/window.ui:1809
msgid "FEC"
msgstr ""

#: configapp/src/window.ui:1295 configapp/src/window.ui:1821
msgid "Tune Length"
msgstr ""

#: configapp/src/window.ui:1307 configapp/src/window.ui:1833
msgid "Sync Length"
msgstr ""

#: configapp/src/window.ui:1446 configapp/src/window.ui:1735
msgid "fskpsp"
msgstr ""

#: configapp/src/window.ui:1450 configapp/src/window.ui:1930
msgid "p3d"
msgstr ""

#: configapp/src/window.ui:1533
msgid "Differential Decoding"
msgstr ""

#: configapp/src/window.ui:1989 configapp/src/window.ui:2148
msgid "MKISS"
msgstr ""

#: configapp/src/window.ui:1990 configapp/src/window.ui:2220
msgid "KISS"
msgstr ""

#: configapp/src/window.ui:2021
msgid "Interface Name"
msgstr ""

#: configapp/src/window.ui:2033
msgid "Callsign"
msgstr ""

#: configapp/src/window.ui:2045
msgid "IP Address"
msgstr ""

#: configapp/src/window.ui:2057
msgid "Network Mask"
msgstr ""

#: configapp/src/window.ui:2069
msgid "Broadcast Address"
msgstr ""

#: configapp/src/window.ui:2128
msgid "sm0"
msgstr ""

#: configapp/src/window.ui:2129
msgid "sm1"
msgstr ""

#: configapp/src/window.ui:2130
msgid "sm2"
msgstr ""

#: configapp/src/window.ui:2131
msgid "ax0"
msgstr ""

#: configapp/src/window.ui:2163
msgid "File"
msgstr ""

#: configapp/src/window.ui:2175
msgid "Unlink File"
msgstr ""

#: configapp/src/window.ui:2201
msgid "/dev/soundmodem0"
msgstr ""

#: configapp/src/window.ui:2202
msgid "/dev/soundmodem1"
msgstr ""

#: configapp/src/window.ui:2203
msgid "/dev/soundmodem2"
msgstr ""

#: configapp/src/window.ui:2266
msgid "Sound Modem Configurator"
msgstr ""
