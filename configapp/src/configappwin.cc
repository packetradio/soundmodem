#include "configappwin.h"

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <stdexcept>
using std::runtime_error;

#include <string>
using std::string;

#include <sstream>
using std::stringstream;

#include <string>
using std::stod;
using std::stoi;
using std::to_string;

#include <cmath>
using std::modf;

#include <gtkmm.h>
using Glib::ustring;

#include "gettext.h"
#define _(String) gettext (String)

#include <typeinfo>

#include <libxml/tree.h>
#include <libxml/parser.h>

class Xml2BuilderSync {
public:
  Xml2BuilderSync(xmlNodePtr node, Glib::RefPtr<Gtk::Builder> builder, bool& dirty, bool xml2builder=true)
    : node(node), builder(builder), dirty(dirty), xml2builder(xml2builder) {
  }

  string sync(const string& prop, const string& id) {
    Gtk::Widget* widget = nullptr;
    builder->get_widget(id, widget);
    string text;
    string text_new;
    if (widget) {

        xmlChar* pt = xmlGetProp(node, (xmlChar*)prop.c_str());
        if (pt) text = (char*)pt;

        if (dynamic_cast<Gtk::ComboBoxText*>(widget)) {
            if (xml2builder)
              dynamic_cast<Gtk::ComboBoxText*>(widget)->set_active_text(text);
            else
              text_new = dynamic_cast<Gtk::ComboBoxText*>(widget)->get_active_text();
          }
        else if (dynamic_cast<Gtk::Switch*>(widget)) {
            if (xml2builder)
              dynamic_cast<Gtk::Switch*>(widget)->set_state(stoi(text));
            else
              text_new = dynamic_cast<Gtk::Switch*>(widget)->get_state()?"1":"0";
          }
        else if (dynamic_cast<Gtk::SpinButton*>(widget)) {
            if (xml2builder)
              dynamic_cast<Gtk::SpinButton*>(widget)->set_value(stod(text));
            else {
                double val = dynamic_cast<Gtk::SpinButton*>(widget)->get_value();
                double ival;
                if (0.0 == modf(val, &ival))
                  text_new = to_string(int(ival));
                else
                  text_new = to_string(val);
              }
          }
        else if (dynamic_cast<Gtk::Entry*>(widget)) {
            if (xml2builder)
              dynamic_cast<Gtk::Entry*>(widget)->set_text(text);
            else
              text_new =  dynamic_cast<Gtk::Entry*>(widget)->get_text();
          }
        else if (dynamic_cast<Gtk::FileChooserButton*>(widget)) {
            if (xml2builder)
              dynamic_cast<Gtk::FileChooserButton*>(widget)->set_filename(text);
            else
              text_new = dynamic_cast<Gtk::FileChooserButton*>(widget)->get_filename();
          }
        else {
            cerr << "Missing sync handler for: " << id << endl;
          }
        if (!xml2builder) {
            if (text_new != text) {
                dirty = true;
                xmlSetProp(node, (xmlChar*)prop.c_str(), (xmlChar*)text_new.c_str());
                text = text_new;
              }
          }
      }
    return text;
  }

private:
  xmlNodePtr node;
  Glib::RefPtr<Gtk::Builder> builder;
  bool& dirty;
  bool xml2builder;
};

// treestore columns
enum {
  NAME_COLUMN,
  NODEPTR_COLUMN,
  END_COLUMN
};

ConfigAppWin::ConfigAppWin(BaseObjectType* cobject,
                           const Glib::RefPtr<Gtk::Builder>& refBuilder)
  : Gtk::ApplicationWindow(cobject),
    m_visible_node(nullptr),
    m_refBuilder(refBuilder),
    m_header(nullptr),
    m_gears(nullptr),
    m_popup(nullptr),
    m_treeview(nullptr),
    m_mainstack(nullptr),
    m_iomode(nullptr),
    m_iostack(nullptr),
    m_modmode(nullptr),
    m_modstack(nullptr),
    m_demodmode(nullptr),
    m_demodstack(nullptr),
    m_pktmode(nullptr),
    m_pktstack(nullptr),
    m_doc(nullptr)
{
  m_refBuilder->get_widget("header", m_header);
  if (!m_header)
    throw std::runtime_error("No \"header\" in window.ui");

  m_refBuilder->get_widget("gears", m_gears);
  if (!m_gears)
    throw std::runtime_error("No \"gears\" in window.ui");

  m_refBuilder->get_widget("treeview", m_treeview);
  if (!m_treeview)
    throw std::runtime_error("No \"treeview\" in window.ui");

  m_refBuilder->get_widget("mainstack", m_mainstack);
  if(!m_mainstack)
    throw std::runtime_error("No \"mainstack\" in window.ui");

  m_treeview->get_selection()->signal_changed().connect(
        sigc::mem_fun(*this, &ConfigAppWin::on_selection_changed));

  auto cell = dynamic_cast<Gtk::CellRendererText*>(
        m_treeview->get_column(NAME_COLUMN)->get_first_cell());
  if (cell)
    cell->signal_edited().connect(
          sigc::mem_fun(*this, &ConfigAppWin::on_tree_edited));

  m_treeview->signal_button_press_event().connect(
        sigc::mem_fun(*this, &ConfigAppWin::on_treeview_button_press_event), false);

  Glib::RefPtr<Glib::Object> obj;
  obj = m_refBuilder->get_object("treestore");
  m_treestore = Glib::RefPtr<Gtk::TreeStore>::cast_dynamic(obj);
  if (!m_treestore)
    throw std::runtime_error("No \"treestore\" object in window.ui");

  m_refBuilder->get_widget("mod.mode", m_modmode);
  if (m_modmode)
    m_modmode->signal_changed().connect_notify(
          sigc::mem_fun(*this, &ConfigAppWin::on_modmode_changed)
          );

  m_refBuilder->get_widget("modstack", m_modstack);
  if(!m_modstack)
    throw std::runtime_error("no \"modstack\" in window.ui");

  m_refBuilder->get_widget("demod.mode", m_demodmode);
  if (m_demodmode)
    m_demodmode->signal_changed().connect_notify(
          sigc::mem_fun(*this, &ConfigAppWin::on_demodmode_changed)
          );

  m_refBuilder->get_widget("demodstack", m_demodstack);
  if(!m_demodstack)
    throw std::runtime_error("No \"demodstack\" in window.ui");

  m_refBuilder->get_widget("pkt.mode", m_pktmode);
  if (m_pktmode)
    m_pktmode->signal_changed().connect_notify(
          sigc::mem_fun(*this, &ConfigAppWin::on_pktmode_changed)
          );

  m_refBuilder->get_widget("pktstack", m_pktstack);
  if(!m_pktstack)
    throw std::runtime_error("No \"pktstack\" in window.ui");

  m_refBuilder->get_widget("iomode", m_iomode);
  if (m_iomode)
    m_iomode->signal_changed().connect_notify(
          sigc::mem_fun(*this, &ConfigAppWin::on_iomode_changed)
          );

  m_refBuilder->get_widget("iostack", m_iostack);
  if(!m_iostack)
    throw std::runtime_error("No \"iostack\" in window.ui");

  auto menu_builder = Gtk::Builder::create_from_resource(
        "/at/blackspace/soundmodem/configapp/gears_menu.ui");
  auto object = menu_builder->get_object("menu");
  auto menu = Glib::RefPtr<Gio::MenuModel>::cast_dynamic(object);
  if (!menu)
    throw std::runtime_error("No \"menu\" in gears_menu.ui");
  m_gears->set_menu_model(menu);
  m_gears->signal_pressed().connect(sigc::mem_fun(*this, &ConfigAppWin::update_dirty));

  m_refBuilder->get_widget("popup", m_popup);
  if (!m_popup)
      std::cerr << "No \"popup\" object in gears_menu.ui" << std::endl;

  m_save_action = add_action(
        "save", sigc::mem_fun(*this, &ConfigAppWin::save));
  m_save_action->set_enabled(false);

  m_save_as_action = add_action(
        "save_as", sigc::mem_fun(*this, &ConfigAppWin::save_as));
  m_save_as_action->set_enabled(false);

  m_new_configuration_action = add_action(
        "new_configuration", sigc::mem_fun(*this, &ConfigAppWin::new_configuration));
  m_new_configuration_action->set_enabled(true);

  m_new_channel_action = add_action(
        "new_channel", sigc::mem_fun(*this, &ConfigAppWin::new_channel));
  m_new_channel_action->set_enabled(false);

  m_delete_item_action = add_action(
        "delete_item", sigc::mem_fun(*this, &ConfigAppWin::delete_item));
  m_delete_item_action->set_enabled(false);

  set_icon_name("modem-symbolic");

  //set_show_menubar();
  xmlKeepBlanksDefault(0);
  xmlIndentTreeOutput = 1;
}

//create from builder resource
ConfigAppWin* ConfigAppWin::create()
{
  auto refBuilder = Gtk::Builder::create_from_resource(
        "/at/blackspace/soundmodem/configapp/window.ui");

  ConfigAppWin* window = nullptr;
  refBuilder->get_widget_derived("app_window", window);
  if (!window)
    throw std::runtime_error("No \"app_window\" object in window.ui");

  return window;
}

void
ConfigAppWin::on_tree_edited(const ustring& path, const ustring& new_text) {
  auto iter = m_treestore->get_iter(path);
  if (iter)  {
      xmlNodePtr node(nullptr);
      iter->get_value(NODEPTR_COLUMN, node);
      xmlChar* text = xmlGetProp(node, BAD_CAST("name"));
      if (!xmlStrEqual(text, BAD_CAST(new_text.c_str()))) {
          iter->set_value(NAME_COLUMN, new_text);
          xmlSetProp(node, BAD_CAST("name"), BAD_CAST(new_text.c_str()));
          mark_dirty();
        }
    }
}

void
ConfigAppWin::new_configuration() {

  // append a new configuration and specify default values

  string conf_name = "New Configuration";
  Gtk::TreeIter conf_iter = m_treestore->append();
  conf_name += string(" ") + m_treestore->get_path(conf_iter).to_string();
  conf_iter->set_value(NAME_COLUMN, conf_name);

  xmlNodePtr new_configuration = xmlNewNode(nullptr, BAD_CAST("configuration"));
  xmlSetProp(new_configuration, BAD_CAST("name"),    BAD_CAST(conf_name.c_str()));
  xmlAddChild(m_doc->children, new_configuration);
  xmlNodePtr new_audio = xmlNewNode(nullptr,         BAD_CAST("audio"));
  xmlAddChild(new_configuration, new_audio);
  xmlSetProp(new_audio, BAD_CAST("type"),               BAD_CAST("alsa"));
  xmlSetProp(new_audio, BAD_CAST("device"),             BAD_CAST("default"));
  xmlSetProp(new_audio, BAD_CAST("halfdup"),            BAD_CAST("0"));
  xmlSetProp(new_audio, BAD_CAST("capturechannelmode"), BAD_CAST("Mono"));

  xmlNodePtr new_ptt = xmlNewNode(nullptr,       BAD_CAST("ptt"));
  xmlAddChild(new_configuration, new_ptt);
  xmlSetProp(new_ptt, BAD_CAST("file"),          BAD_CAST("/dev/ttyUSB0"));
  xmlSetProp(new_ptt, BAD_CAST("hamlib_model"),  BAD_CAST(""));
  xmlSetProp(new_ptt, BAD_CAST("hamlib_params"), BAD_CAST(""));

  xmlNodePtr new_chaccess = xmlNewNode(nullptr, BAD_CAST("chaccess"));
  xmlAddChild(new_configuration, new_chaccess);
  xmlSetProp(new_chaccess, BAD_CAST("txdelay"),  BAD_CAST("150"));
  xmlSetProp(new_chaccess, BAD_CAST("slottime"), BAD_CAST("100"));
  xmlSetProp(new_chaccess, BAD_CAST("ppersist"), BAD_CAST("40"));
  xmlSetProp(new_chaccess, BAD_CAST("fulldup"),  BAD_CAST("0"));
  xmlSetProp(new_chaccess, BAD_CAST("txtail"),   BAD_CAST("10"));

  conf_iter->set_value(NODEPTR_COLUMN, new_configuration);

  mark_dirty();

  m_treeview->get_selection()->select(conf_iter);
}

void
ConfigAppWin::new_channel() {

  // append a new channel and specify default values

  auto selected_iter = m_treeview->get_selection()->get_selected();
  auto selected_path = m_treestore->get_path(selected_iter);
  Gtk::TreeIter conf_iter;
  if (m_treestore->get_path(selected_iter).size() == 1)
    conf_iter  = selected_iter;
  else
    conf_iter = selected_iter->parent();

  xmlNodePtr conf_node(nullptr);
  conf_iter->get_value(NODEPTR_COLUMN, conf_node);

  string channel_name = "New Channel";
  Gtk::TreeIter channel_iter = m_treestore->append(conf_iter->children());
  channel_name += string(" ") + m_treestore->get_path(channel_iter).to_string();
  channel_iter->set_value(NAME_COLUMN, channel_name);

  xmlNodePtr new_channel = xmlNewNode(nullptr, BAD_CAST("channel"));
  xmlSetProp(new_channel, BAD_CAST("name"), BAD_CAST(channel_name.c_str()));
  xmlAddChild(conf_node, new_channel);

  xmlNodePtr new_mod = xmlNewNode(nullptr,   BAD_CAST("mod"));
  xmlAddChild(new_channel, new_mod);
  xmlSetProp(new_mod, BAD_CAST("mode"),      BAD_CAST("Off"));

  xmlNodePtr new_demod = xmlNewNode(nullptr, BAD_CAST("demod"));
  xmlAddChild(new_channel, new_demod);
  xmlSetProp(new_demod, BAD_CAST("mode"),    BAD_CAST("Off"));

  xmlNodePtr new_pkt = xmlNewNode(nullptr,   BAD_CAST("pkt"));
  xmlAddChild(new_channel, new_pkt);
  xmlSetProp(new_pkt, BAD_CAST("mode"),      BAD_CAST("MKISS"));
  xmlSetProp(new_pkt, BAD_CAST("ifname"),    BAD_CAST("ax0"));
  xmlSetProp(new_pkt, BAD_CAST("hwaddr"),    BAD_CAST("MYCALL"));
  xmlSetProp(new_pkt, BAD_CAST("ip"),        BAD_CAST("10.0.0.1"));
  xmlSetProp(new_pkt, BAD_CAST("netmask"),   BAD_CAST("255.255.255.0"));
  xmlSetProp(new_pkt, BAD_CAST("broadcast"), BAD_CAST("10.0.0.255"));

  channel_iter->set_value(NODEPTR_COLUMN, new_channel);

  mark_dirty();

  auto cp = m_treestore->get_path(channel_iter);
  m_treeview->expand_to_path(m_treestore->get_path(channel_iter));
  m_treeview->get_selection()->select(channel_iter);
}

void
ConfigAppWin::delete_item() {
  auto selected_iter = m_treeview->get_selection()->get_selected();
  if (selected_iter) {
      ustring name;
      selected_iter->get_value(NAME_COLUMN, name);
      stringstream message;
      message << "Delete " << name << " ?";
      Gtk::MessageDialog dlg(message.str().c_str(), false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
      if (Gtk::RESPONSE_YES == dlg.run()) {
          xmlNodePtr selected_node(nullptr);
          selected_iter->get_value(NODEPTR_COLUMN, selected_node);
          if (selected_node) {
              xmlUnlinkNode(selected_node);
              selected_iter->set_value(NODEPTR_COLUMN, xmlNodePtr(nullptr));
              xmlFreeNode(selected_node);
            }
          if (m_visible_node == selected_node)
            m_visible_node = nullptr;
          m_treestore->erase(selected_iter);
          mark_dirty();
        }
    }
}

void
ConfigAppWin::sync_xml_with_dlg(xmlNodePtr node, bool xml2dlg) {
  bool dirty(false);
    if (node) {
        if (xmlStrEqual(node->name, BAD_CAST("configuration"))) {
            if (xml2dlg) m_mainstack->set_visible_child("configuration",
                Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
            for (xmlNodePtr child = node->children; child; child = child->next) {
                if (xmlStrEqual(child->name, BAD_CAST("audio"))) {
                    Xml2BuilderSync audio(child, m_refBuilder, dirty, xml2dlg);
                    string mode = audio.sync("type", "iomode");
                    if ("alsa" == mode) {
                        audio.sync("device",             "alsa.device");
                        audio.sync("halfdup",            "alsa.halfdup");
                        audio.sync("capturechannelmode", "alsa.capturechannelmode");
                      }
                    else if ("file" == mode) {
                        audio.sync("file",   "file.path");
                        audio.sync("repeat", "file.repeat");
                      }
                    else if ("soundcard" == mode) {
                        audio.sync("device",  "soundcard.device");
                        audio.sync("halfdup", "soundcard.halfdup");
                      }
                    else if ("simulation" == mode) {
                        audio.sync("simchan","simulation.simchan");
                        audio.sync("snr",    "simulation.snr");
                        audio.sync("snrmode","simulation.snrmode");
                        audio.sync("srate",  "simulation.srate");
                      }
                  }
                else if (xmlStrEqual(child->name, BAD_CAST("ptt"))) {
                    Xml2BuilderSync ptt(child, m_refBuilder, dirty, xml2dlg);
                    ptt.sync("file",          "ptt.file");
                    ptt.sync("hamlib_model",  "ptt.hamlib_model");
                    ptt.sync("hamlib_params", "ptt.hamlib_params");
                  }
                else if (xmlStrEqual(child->name, BAD_CAST("chaccess"))) {
                    Xml2BuilderSync chaccess(child, m_refBuilder, dirty, xml2dlg);
                    chaccess.sync("txdelay","chaccess.txdelay");
                    chaccess.sync("slottime","chaccess.slottime");
                    chaccess.sync("ppersist","chaccess.ppersist");
                    chaccess.sync("fulldup","chaccess.fulldup");
                    chaccess.sync("txtail","chaccess.txtail");
                  }
              }
          }
        else if (xmlStrEqual(node->name, BAD_CAST("channel"))) {
            if (xml2dlg) m_mainstack->set_visible_child("channel",
                Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
            for (xmlNodePtr child = node->children; child; child = child->next) {
                if (xmlStrEqual(child->name, BAD_CAST("mod"))) {
                    Xml2BuilderSync mod(child, m_refBuilder, dirty, xml2dlg);
                    string mode = mod.sync("mode","mod.mode");
                    if ("Off" == mode) {

                      }
                    else if ("afsk" == mode) {
                        mod.sync("bps","mod.afsk.bps");
                        mod.sync("f0","mod.afsk.f0");
                        mod.sync("f1","mod.afsk.f1");
                        mod.sync("diffenc","mod.afsk.diffenc");
                      }
                    else if ("fsk" == mode) {
                        mod.sync("bps","mod.fsk.bps");
                        mod.sync("filter","mod.fsk.filter");
                      }
                    else if ("pam" == mode) {
                      }
                    else if ("psk" == mode) {
                      }
                    else if ("newqpsk" == mode) {
                        mod.sync("bps","mod.newqpsk.bps");
                        mod.sync("inlv","mod.newqpsk.inlv");
                        mod.sync("fec","mod.newqpsk.fec");
                        mod.sync("tunelen","mod.newqpsk.tunelen");
                        mod.sync("synclen","mod.newqpsk.synclen");
                      }
                  }
                else if (xmlStrEqual(child->name, BAD_CAST("demod"))) {
                    Xml2BuilderSync demod(child, m_refBuilder, dirty, xml2dlg);
                    string mode = demod.sync("mode","demod.mode");
                    if ("Off" == mode) {

                      }
                    else if ("afsk" == mode) {
                        demod.sync("bps","demod.afsk.bps");
                        demod.sync("f0","demod.afsk.f0");
                        demod.sync("f1","demod.afsk.f1");
                        demod.sync("diffdec","demod.afsk.diffdec");
                      }
                    else if ("fsk" == mode) {
                        demod.sync("bps","demod.fsk.bps");
                        demod.sync("filter","demod.fsk.filter");
                      }
                    else if ("fskpsp" == mode) {
                        demod.sync("bps","demod.fskpsp.bps");
                        demod.sync("filter","demod.fskpsp.filter");
                      }
                    else if ("pam" == mode) {
                      }
                    else if ("psk" == mode) {
                      }
                    else if ("newqpsk" == mode) {
                        demod.sync("bps","demod.newqpsk.bps");
                        demod.sync("inlv","demod.newqpsk.inlv");
                        demod.sync("fec","demod.newqpsk.fec");
                        demod.sync("mintune","demod.newqpsk.mintune");
                        demod.sync("minsync","demod.newqpsk.minsync");
                      }
                    else if ("p3d" == mode) {
                      }
                  }
                else if (xmlStrEqual(child->name, BAD_CAST("pkt"))) {
                    Xml2BuilderSync pkt(child, m_refBuilder, dirty, xml2dlg);
                    string mode = pkt.sync("mode","pkt.mode");
                    if ("MKISS" == mode) {
                        pkt.sync("ifname","pkt.MKISS.ifname");
                        pkt.sync("hwaddr","pkt.MKISS.hwaddr");
                        pkt.sync("ip","pkt.MKISS.ip");
                        pkt.sync("netmask","pkt.MKISS.netmask");
                        pkt.sync("broadcast","pkt.MKISS.broadcast");
                      }
                    else if ("KISS" == mode) {
                        pkt.sync("file","pkt.KISS.file");
                        pkt.sync("unlink","pkt.KISS.unlink");
                      }
                  }
              }
          }
        else {
            cerr << "unknown nodename " << node->name << endl;
          }
      }

    if (dirty) mark_dirty();
}

void
ConfigAppWin::on_selection_changed() {

  auto iter = m_treeview->get_selection()->get_selected();
  if (iter) {
      if (m_visible_node) sync_xml_with_dlg(m_visible_node, false);
      xmlNodePtr node(nullptr);
      iter->get_value(NODEPTR_COLUMN, node);
      sync_xml_with_dlg(node, true);
      m_visible_node = node;

      if (m_treestore->get_path(iter).size() > 0) {
          m_new_configuration_action->set_enabled(true);
          m_new_channel_action->set_enabled(true);
          m_delete_item_action->set_enabled(true);
        }
      else {
          m_new_configuration_action->set_enabled(true);
          m_new_channel_action->set_enabled(false);
          m_delete_item_action->set_enabled(false);
        }
    }
  else {
      m_new_configuration_action->set_enabled(true);
      m_new_channel_action->set_enabled(false);
      m_delete_item_action->set_enabled(false);
    }
}

bool
ConfigAppWin::on_treeview_button_press_event(GdkEventButton* button_event) {
  if  ( (button_event->type == GDK_BUTTON_PRESS) && (button_event->button == 3) ) {
    if (!m_popup->get_attach_widget())
      m_popup->attach_to_widget(*this);
    m_popup->popup(button_event->button, button_event->time);
  }
  return false;
}

void
ConfigAppWin::on_modmode_changed() {
  ustring active = m_modmode->get_active_text();
  m_modstack->set_visible_child(active,
    Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
}

void
ConfigAppWin::on_demodmode_changed() {
  ustring active = m_demodmode->get_active_text();
  m_demodstack->set_visible_child(active,
    Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
}

void
ConfigAppWin::on_pktmode_changed() {
  ustring active = m_pktmode->get_active_text();
  m_pktstack->set_visible_child(active,
    Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
}

void
ConfigAppWin::on_iomode_changed() {
  ustring active = m_iomode->get_active_text();
  m_iostack->set_visible_child(active,
    Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
}

void
ConfigAppWin::mark_dirty() {
  m_save_action->set_enabled(true);
  m_save_as_action->set_enabled(true);
  stringstream subtitle;
  subtitle << m_file->get_path() << " *";
  m_header->set_subtitle(subtitle.str().c_str());
}

void
ConfigAppWin::mark_clean() {
  m_save_action->set_enabled(false);
  m_save_as_action->set_enabled(false);
  stringstream subtitle;
  subtitle << m_file->get_path();
  m_header->set_subtitle(subtitle.str().c_str());
}

bool
ConfigAppWin::is_dirty() {
  update_dirty();
  return m_save_action->get_enabled();
}

void
ConfigAppWin::update_dirty() {
  sync_xml_with_dlg(m_visible_node, false);
}

void
ConfigAppWin::open_file(const Glib::RefPtr<Gio::File>& file) {
  m_file = file;
  m_doc = xmlParseFile(m_file->get_path().c_str());
  m_header->set_subtitle(m_file->get_path());

  if (m_doc && m_doc->children && xmlStrEqual(m_doc->children->name, BAD_CAST("modem"))) {
      for (xmlNodePtr conf_ptr = m_doc->children->children; conf_ptr; conf_ptr = conf_ptr->next) {
          if (xmlStrEqual(conf_ptr->name, BAD_CAST("configuration"))) {
              xmlChar* conf_name = xmlGetProp(conf_ptr, BAD_CAST("name"));
              Gtk::TreeIter conf_iter = m_treestore->append();
              conf_iter->set_value(NAME_COLUMN, ustring((char*)conf_name));
              conf_iter->set_value(NODEPTR_COLUMN, conf_ptr);
              for (xmlNodePtr channel_ptr = conf_ptr->children; channel_ptr; channel_ptr = channel_ptr->next) {
                  if (xmlStrEqual(channel_ptr->name, BAD_CAST("channel"))) {
                      xmlChar* chnl_name = xmlGetProp(channel_ptr, BAD_CAST("name"));
                      Gtk::TreeIter channel_iter = m_treestore->append(conf_iter->children());
                      channel_iter->set_value(NAME_COLUMN, ustring((char*)chnl_name));
                      channel_iter->set_value(NODEPTR_COLUMN, channel_ptr);
                    }
                }
            }
        }
    }
  else {
      stringstream msg;
      msg << _("Could not open config file") << " " << m_file->get_path().c_str();
      throw runtime_error(msg.str().c_str());
    }
}

void
ConfigAppWin::save() {

  sync_xml_with_dlg(m_visible_node, false);
  if (0 > xmlSaveFormatFile(m_file->get_path().c_str(), m_doc, 1)) {
      xmlErrorPtr err = xmlGetLastError();
      stringstream msg;
      msg << "Settings could not be saved!" << std::endl << err->message;
      Gtk::MessageDialog error(
            *this, msg.str().c_str(), false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
      error.run();
    }
  else
    mark_clean();
}

void
ConfigAppWin::save_as() {

  sync_xml_with_dlg(m_visible_node, false);

  auto conffiles = Gtk::FileFilter::create();
  conffiles->set_name("Config Files");
  conffiles->add_pattern("*.conf");

  auto allfiles = Gtk::FileFilter::create();
  allfiles->set_name("All Files");
  allfiles->add_pattern("*");

  Gtk::FileChooserDialog saver(
        *this, "Save configuration as file", Gtk::FILE_CHOOSER_ACTION_SAVE);
  saver.set_do_overwrite_confirmation(true);
  saver.add_button("Save", Gtk::RESPONSE_ACCEPT);
  saver.add_button("Cancel", Gtk::RESPONSE_CANCEL);
  saver.add_filter(conffiles);
  saver.add_filter(allfiles);

  if (Gtk::RESPONSE_ACCEPT == saver.run()) {
      m_file = saver.get_file();
      save();
    }
}

bool ConfigAppWin::on_delete_event(GdkEventAny* any_event) {
  (void)any_event;

  if (is_dirty()) {
    stringstream msg;
    msg << "File " << m_file->get_path() << " is unsaved! Really quit?";
    Gtk::MessageDialog dlg(
          *this, msg.str().c_str(),false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
    if (Gtk::RESPONSE_YES != dlg.run())
      return true;
    }
  xmlFreeDoc(m_doc);
  m_doc = nullptr;
  return false;
}
