#ifndef CONFIGAPP_H
#define CONFIGAPP_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gtkmm.h>

class ConfigAppWin;

class ConfigApp : public Gtk::Application
{
protected:
  ConfigApp();

public:
  static Glib::RefPtr<ConfigApp> create();

protected:
  void on_startup() override;
  void on_activate() override;
  void on_open(const Gio::Application::type_vec_files& files,
               const Glib::ustring& hint) override;

private:
  ConfigAppWin* create_appwindow();
  void on_hide_window(Gtk::Window* window);
  void on_action_preferences();
  void on_action_quit();
  void on_action_about();
};


#endif /* CONFIGAPP_H */
