#include "config.h"
#include "gettext.h"

#include "configapp.h"

#ifndef PROGRAMNAME_LOCALEDIR
#define PROGRAMNAME_LOCALEDIR "~/.local/share/locale"
#endif

int main(int argc, char *argv[])
{

  setlocale(LC_ALL, "");
  bindtextdomain(PACKAGE, LOCALEDIR);
  textdomain(PACKAGE);

  auto app = ConfigApp::create();
  return app->run(argc, argv);
}
