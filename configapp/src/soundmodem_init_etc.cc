#include <config.h>

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <exception>
using std::exception;
#include <stdexcept>
using std::runtime_error;

#include <filesystem>
using std::filesystem::path;
using std::filesystem::exists;

#include <fstream>
using std::fstream;
using std::ios;

#include <string>
using std::string;

#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[]) {
  if (argc > 1) {
      for (int n=0; n < argc; ++n) {
          if ("--version" == string(argv[n])) {
              cout << "soundmodem_init_etc " << PACKAGE_VERSION << endl;
              return 0;
            }
          if ("--help" == string(argv[n])) {
              cout << "soundmodem_init_etc [ --version | --help ]" << endl
                   << "This program is a little setuid helper which must be running as root" << endl
                   << "to  establish  prerequisites  for the  userspace  soundmodem  driver."  << endl
                   << "Please make  sure that your account is member of the \"dialout\" group." << endl;
              return 0;
            }
        }
      cerr << "unknown option, try --help" << endl;
      return -1; // dont allow anything else
    }

  struct group* gr = getgrnam("dialout");
  gid_t gr_gid = 20; // use this as default value
  if (gr) gr_gid = gr->gr_gid;

  umask(S_IWOTH);

  try {
    path    conf_path("/etc/ax25/soundmodem.conf");
    fstream conf_file;
    conf_file.open(conf_path, ios::in|ios::out); // test read write
    if (!conf_file.is_open()) {
        conf_file.clear();
        conf_file.exceptions(ios::failbit);
        conf_file.open(conf_path, ios::out); // try to create it
        conf_file << "<?xml version=\"1.0\"?>\n<modem/>";
      }
    conf_file.close();
    if ( 0 != chown(conf_path.c_str(), 0, gr_gid) ||
         0 != chmod(conf_path.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH)) {
        throw runtime_error(strerror(errno));
      }
  } catch(exception& e) {
    cerr << "exception: " << e.what() << endl;
    return -1;
  }

  return 0;
}
