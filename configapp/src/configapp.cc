#include "configapp.h"
#include "configappwin.h"
#include <glibmm/i18n.h>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>
#include <vector>

#include "config.h"

// The prefix must be defined in the Makefile, this is only a fallback!!
#ifndef SBINDIR
#define SBINDIR "~/.local/sbin"
#endif

ConfigApp::ConfigApp()
  : Gtk::Application("at.blackspace.soundmodem.configapp",
                     Gio::APPLICATION_HANDLES_OPEN)
{
}

Glib::RefPtr<ConfigApp> ConfigApp::create()
{
  return Glib::RefPtr<ConfigApp>(new ConfigApp());
}

ConfigAppWin* ConfigApp::create_appwindow()
{
  auto appwindow = ConfigAppWin::create();
  //add_window(*appwindow);

  appwindow->signal_hide().connect(
        sigc::bind<Gtk::Window*>(
          sigc::mem_fun(*this, &ConfigApp::on_hide_window),
          appwindow));

  return appwindow;
}

void ConfigApp::on_startup()
{
  Gtk::Application::on_startup();

  //add_action("preferences", sigc::mem_fun(*this, &ConfigApp::on_action_preferences));
  add_action("quit", sigc::mem_fun(*this, &ConfigApp::on_action_quit));
  add_action("about", sigc::mem_fun(*this, &ConfigApp::on_action_about));
  set_accel_for_action("app.quit", "<Ctrl>Q");
  set_accel_for_action("win.save", "<Ctrl>S");
  //set_accel_for_action("win.delete_item", "Delete"); // It is not a good ide to set this globally

  auto refBuilder = Gtk::Builder::create();
  try {
    refBuilder->add_from_resource("/at/blackspace/soundmodem/configapp/app_menu.ui");
  }  catch (const Glib::Error& ex) {
    std::cerr << "ConfigApp::on_startup(): " << ex.what() << std::endl;
    return;
  }

  auto object = refBuilder->get_object("appmenu");
  auto app_menu = Glib::RefPtr<Gio::MenuModel>::cast_dynamic(object);
  if (app_menu)
    set_app_menu(app_menu);
  else
    std::cerr << "ConfigApp::on_startup(): No \"appmenu\" object in app_menu.ui" << std::endl;
}

void ConfigApp::on_activate()
{
  std::string error_message;

  try  {

    auto file = Gio::File::create_for_path("/etc/ax25/soundmodem.conf");

    bool need_init = true;
    if (file->query_exists()) {
        auto info = file->query_info();
        if (info) {
            if (info->get_attribute_boolean("access::can-read") &&
                   info->get_attribute_boolean("access::can-write"))
                need_init = false;
          }
      }

    if (need_init) {
        std::cout << SBINDIR"/soundmodem_init_etc" << std::endl;
        std::vector<std::string> argv {SBINDIR"/soundmodem_init_etc"};
        int exit_status = 0;
        std::string init_stdout;
        std::string init_stderr;
        Glib::spawn_sync(
              "/etc/ax25",
              argv,
              Glib::SPAWN_DEFAULT,
              Glib::SlotSpawnChildSetup(),
              &init_stdout,
              &init_stderr,
              &exit_status);
        if (0 != exit_status)
            throw std::runtime_error(init_stderr);
      }

    auto appwindow = create_appwindow();
    appwindow->open_file(file);
    add_window(*appwindow);
    appwindow->present();
  }  catch (const Glib::Error& ex) {
    error_message = ex.what();
    std::cerr << "ConfigApp::on_activate(): " << error_message << std::endl;
  }  catch (const std::exception& ex) {
    error_message = ex.what();
    std::cerr << "ConfigApp::on_activate(): " << error_message << std::endl;
  }

  if (!error_message.empty()) {
      Gtk::MessageDialog error_dlg(error_message, false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
      error_dlg.run();
    }
}

void ConfigApp::on_open(const Gio::Application::type_vec_files &files,
                        const Glib::ustring & /*hint*/)
{
  std::string error_message;
  try  {
    for (const auto& file : files) {
      auto appwindow = create_appwindow();
      appwindow->open_file(file);
      add_window(*appwindow);
      appwindow->present();
      }
  }  catch (const Glib::Error& ex) {
    error_message = ex.what();
    std::cerr << "ConfigApp::on_open(): " << error_message << std::endl;
  } catch (const std::exception& ex) {
    error_message = ex.what();
    std::cerr << "ConfigApp::on_open(): " << error_message << std::endl;
  }
  if (!error_message.empty()) {
      Gtk::MessageDialog error_dlg(error_message, false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
      error_dlg.run();
    }
}

void ConfigApp::on_hide_window(Gtk::Window *window)
{
  delete window;
}

void ConfigApp::on_action_preferences()
{
  std::cout << "choosing preferences" << std::endl;
}

void ConfigApp::on_action_about() {
  auto refBuilder = Gtk::Builder::create();
  refBuilder->add_from_resource("/at/blackspace/soundmodem/configapp/window.ui");
  Gtk::AboutDialog* about(nullptr);
  refBuilder->get_widget("about", about);
  if (!about)
    throw std::runtime_error("No \"about\" in window.ui");
  about->set_version(PACKAGE_VERSION);
  about->run();
  about->hide();
}

void ConfigApp::on_action_quit()
{
  auto windows = get_windows();
  for (auto window : windows)
      window->close();
  //quit();
}
