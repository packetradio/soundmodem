#ifndef CONFIGAPPWIN_H
#define CONFIGAPPWIN_H

#include <string>

#include <gtkmm.h>
#include <libxml/tree.h>

class ConfigAppWin : public Gtk::ApplicationWindow
{
public:
  ConfigAppWin(BaseObjectType* cobject,
               const Glib::RefPtr<Gtk::Builder>& refBuilder);

  static ConfigAppWin* create();

  void open_file(const Glib::RefPtr<Gio::File>& file);
  bool close_file();
  void save();
  void save_as();

  void new_configuration();
  void new_channel();
  void delete_item();

private:
  //void sync_xml_with_dlg(Gtk::TreeIter& iter, bool xml2dlg=true);
  void sync_xml_with_dlg(xmlNodePtr node, bool xml2dlg=true);
  void mark_dirty();
  void mark_clean();
  void update_dirty();
  bool is_dirty();

private:
  void on_tree_edited(const Glib::ustring& path, const Glib::ustring& new_text);
  void on_selection_changed();
  void on_iomode_changed();
  void on_modmode_changed();
  void on_demodmode_changed();
  void on_pktmode_changed();
  bool on_delete_event(GdkEventAny* any_event) override;
  bool on_treeview_button_press_event(GdkEventButton* button_event);

private:
  xmlNodePtr m_visible_node;
  Glib::RefPtr<Gtk::Builder> m_refBuilder;
  Glib::RefPtr<Gio::SimpleAction> m_new_channel_action;
  Glib::RefPtr<Gio::SimpleAction> m_new_configuration_action;
  Glib::RefPtr<Gio::SimpleAction> m_delete_item_action;
  Glib::RefPtr<Gio::SimpleAction> m_save_action;
  Glib::RefPtr<Gio::SimpleAction> m_save_as_action;
  Gtk::HeaderBar* m_header;
  Gtk::MenuButton* m_gears;
  Gtk::Menu* m_popup;
  Gtk::TreeView* m_treeview;
  Gtk::Stack* m_mainstack;
  Gtk::ComboBoxText* m_iomode;
  Gtk::Stack* m_iostack;
  Gtk::ComboBoxText* m_modmode;
  Gtk::Stack* m_modstack;
  Gtk::ComboBoxText* m_demodmode;
  Gtk::Stack* m_demodstack;
  Gtk::ComboBoxText* m_pktmode;
  Gtk::Stack* m_pktstack;
  Glib::RefPtr<Gtk::TreeStore> m_treestore;
  Glib::RefPtr<Gio::File> m_file;
  xmlDocPtr m_doc;
};

#endif // CONFIGAPPWIN_H
